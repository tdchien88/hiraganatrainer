﻿namespace HiraganaTrainer
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MainWindowTabs = new System.Windows.Forms.TabControl();
            this.Trainer = new System.Windows.Forms.TabPage();
            this.TrainerButtonNext = new System.Windows.Forms.Button();
            this.TrainerButtonCheck = new System.Windows.Forms.Button();
            this.TrainerButtonDontKnow = new System.Windows.Forms.Button();
            this.TrainerRomajiInput = new System.Windows.Forms.TextBox();
            this.TrainerHiraganaDisplay = new System.Windows.Forms.Label();
            this.Stats = new System.Windows.Forms.TabPage();
            this.StatsView = new System.Windows.Forms.ListView();
            this.Char = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Attempts = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Rating = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.About = new System.Windows.Forms.TabPage();
            this.AboutLabel = new System.Windows.Forms.Label();
            this.Romaji = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.MainWindowTabs.SuspendLayout();
            this.Trainer.SuspendLayout();
            this.Stats.SuspendLayout();
            this.About.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainWindowTabs
            // 
            this.MainWindowTabs.Controls.Add(this.Trainer);
            this.MainWindowTabs.Controls.Add(this.Stats);
            this.MainWindowTabs.Controls.Add(this.About);
            this.MainWindowTabs.Location = new System.Drawing.Point(12, 12);
            this.MainWindowTabs.Name = "MainWindowTabs";
            this.MainWindowTabs.SelectedIndex = 0;
            this.MainWindowTabs.Size = new System.Drawing.Size(260, 238);
            this.MainWindowTabs.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight;
            this.MainWindowTabs.TabIndex = 0;
            this.MainWindowTabs.SelectedIndexChanged += new System.EventHandler(this.UpdateStats);
            // 
            // Trainer
            // 
            this.Trainer.Controls.Add(this.TrainerButtonNext);
            this.Trainer.Controls.Add(this.TrainerButtonCheck);
            this.Trainer.Controls.Add(this.TrainerButtonDontKnow);
            this.Trainer.Controls.Add(this.TrainerRomajiInput);
            this.Trainer.Controls.Add(this.TrainerHiraganaDisplay);
            this.Trainer.Location = new System.Drawing.Point(4, 22);
            this.Trainer.Name = "Trainer";
            this.Trainer.Padding = new System.Windows.Forms.Padding(3);
            this.Trainer.Size = new System.Drawing.Size(252, 212);
            this.Trainer.TabIndex = 0;
            this.Trainer.Text = "Trainer";
            this.Trainer.UseVisualStyleBackColor = true;
            // 
            // TrainerButtonNext
            // 
            this.TrainerButtonNext.Location = new System.Drawing.Point(125, 175);
            this.TrainerButtonNext.Name = "TrainerButtonNext";
            this.TrainerButtonNext.Size = new System.Drawing.Size(121, 31);
            this.TrainerButtonNext.TabIndex = 4;
            this.TrainerButtonNext.Text = "Next";
            this.TrainerButtonNext.UseVisualStyleBackColor = true;
            this.TrainerButtonNext.Click += new System.EventHandler(this.TrainerButtonNext_Click);
            // 
            // TrainerButtonCheck
            // 
            this.TrainerButtonCheck.Location = new System.Drawing.Point(125, 138);
            this.TrainerButtonCheck.Name = "TrainerButtonCheck";
            this.TrainerButtonCheck.Size = new System.Drawing.Size(121, 31);
            this.TrainerButtonCheck.TabIndex = 3;
            this.TrainerButtonCheck.Text = "Check";
            this.TrainerButtonCheck.UseVisualStyleBackColor = true;
            this.TrainerButtonCheck.Click += new System.EventHandler(this.TrainerButtonCheck_Click);
            // 
            // TrainerButtonDontKnow
            // 
            this.TrainerButtonDontKnow.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TrainerButtonDontKnow.Location = new System.Drawing.Point(6, 175);
            this.TrainerButtonDontKnow.Name = "TrainerButtonDontKnow";
            this.TrainerButtonDontKnow.Size = new System.Drawing.Size(113, 31);
            this.TrainerButtonDontKnow.TabIndex = 2;
            this.TrainerButtonDontKnow.Text = "Don\'t Know";
            this.TrainerButtonDontKnow.UseVisualStyleBackColor = true;
            this.TrainerButtonDontKnow.Click += new System.EventHandler(this.TrainerButtonDontKnow_Click);
            // 
            // TrainerRomajiInput
            // 
            this.TrainerRomajiInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TrainerRomajiInput.Location = new System.Drawing.Point(7, 138);
            this.TrainerRomajiInput.Name = "TrainerRomajiInput";
            this.TrainerRomajiInput.Size = new System.Drawing.Size(112, 31);
            this.TrainerRomajiInput.TabIndex = 1;
            this.TrainerRomajiInput.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TrainerHiraganaDisplay
            // 
            this.TrainerHiraganaDisplay.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TrainerHiraganaDisplay.Font = new System.Drawing.Font("Arial Unicode MS", 68.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TrainerHiraganaDisplay.Location = new System.Drawing.Point(7, 7);
            this.TrainerHiraganaDisplay.Name = "TrainerHiraganaDisplay";
            this.TrainerHiraganaDisplay.Size = new System.Drawing.Size(239, 124);
            this.TrainerHiraganaDisplay.TabIndex = 0;
            this.TrainerHiraganaDisplay.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Stats
            // 
            this.Stats.Controls.Add(this.StatsView);
            this.Stats.Location = new System.Drawing.Point(4, 22);
            this.Stats.Name = "Stats";
            this.Stats.Padding = new System.Windows.Forms.Padding(3);
            this.Stats.Size = new System.Drawing.Size(252, 212);
            this.Stats.TabIndex = 1;
            this.Stats.Text = "Stats";
            this.Stats.UseVisualStyleBackColor = true;
            // 
            // StatsView
            // 
            this.StatsView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Char,
            this.Romaji,
            this.Attempts,
            this.Rating});
            this.StatsView.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StatsView.Location = new System.Drawing.Point(7, 7);
            this.StatsView.Name = "StatsView";
            this.StatsView.Size = new System.Drawing.Size(239, 199);
            this.StatsView.TabIndex = 0;
            this.StatsView.UseCompatibleStateImageBehavior = false;
            this.StatsView.View = System.Windows.Forms.View.Details;
            // 
            // Char
            // 
            this.Char.Text = "Char";
            this.Char.Width = 45;
            // 
            // Attempts
            // 
            this.Attempts.Text = "Attempts";
            this.Attempts.Width = 70;
            // 
            // Rating
            // 
            this.Rating.Text = "Rating";
            this.Rating.Width = 70;
            // 
            // About
            // 
            this.About.Controls.Add(this.AboutLabel);
            this.About.Location = new System.Drawing.Point(4, 22);
            this.About.Name = "About";
            this.About.Size = new System.Drawing.Size(252, 212);
            this.About.TabIndex = 2;
            this.About.Text = "About";
            this.About.UseVisualStyleBackColor = true;
            // 
            // AboutLabel
            // 
            this.AboutLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.AboutLabel.Location = new System.Drawing.Point(7, 7);
            this.AboutLabel.Margin = new System.Windows.Forms.Padding(3);
            this.AboutLabel.Name = "AboutLabel";
            this.AboutLabel.Padding = new System.Windows.Forms.Padding(4);
            this.AboutLabel.Size = new System.Drawing.Size(239, 199);
            this.AboutLabel.TabIndex = 0;
            this.AboutLabel.Text = "Hiragana Trainer V1.0\r\n\r\nCreated by Kylar Grey\r\n\r\nhttp://dakshen.co.uk";
            this.AboutLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Romaji
            // 
            this.Romaji.Text = "Romaji";
            this.Romaji.Width = 50;
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.MainWindowTabs);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MinimizeBox = false;
            this.Name = "MainWindow";
            this.ShowIcon = false;
            this.Text = "Hiragana Trainer";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.WindowClosing);
            this.MainWindowTabs.ResumeLayout(false);
            this.Trainer.ResumeLayout(false);
            this.Trainer.PerformLayout();
            this.Stats.ResumeLayout(false);
            this.About.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl MainWindowTabs;
        private System.Windows.Forms.TabPage Trainer;
        private System.Windows.Forms.TabPage Stats;
        private System.Windows.Forms.TabPage About;
        private System.Windows.Forms.Button TrainerButtonNext;
        private System.Windows.Forms.Button TrainerButtonCheck;
        private System.Windows.Forms.Button TrainerButtonDontKnow;
        private System.Windows.Forms.TextBox TrainerRomajiInput;
        private System.Windows.Forms.Label TrainerHiraganaDisplay;
        private System.Windows.Forms.ListView StatsView;
        private System.Windows.Forms.ColumnHeader Char;
        private System.Windows.Forms.ColumnHeader Rating;
        private System.Windows.Forms.ColumnHeader Attempts;
        private System.Windows.Forms.Label AboutLabel;
        private System.Windows.Forms.ColumnHeader Romaji;
    }
}

