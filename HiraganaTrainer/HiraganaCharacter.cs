﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HiraganaTrainer
{
    public class HiraganaCharacter
    {
        public static int SuccessLevelHistory = 5;

        public string Hiragana { get; private set; }
        public string Romaji { get; private set; }

        //curSuccessLevel is the moving average total, use SuccessLevel for the current average.
        public float curSuccessLevel { get; private set; }
        public int TotalAttempts { get; private set; }
        public float SuccessLevel { get { return curSuccessLevel / (float)SuccessLevelHistory; } }

        public HiraganaCharacter(string hiragana, string romaji, int previousAttempts, float previousSuccessLevel)
        {
            Hiragana = hiragana;
            Romaji = romaji;

            TotalAttempts = previousAttempts;
            curSuccessLevel = previousSuccessLevel;
        }

        /// <summary>
        /// Loads in previous data retrieved from the Stats.dat file.
        /// </summary>
        public void LoadData(int totalAttempts, float curSuccessLevel)
        {
            TotalAttempts = totalAttempts;
            this.curSuccessLevel = curSuccessLevel;
        }

        /// <summary>
        /// Registers a new attempt to identify the character.
        /// </summary>
        /// <param name="result">1 = Correct, 0.5 = Don't Know, 0 = Incorrect</param>
        public void RegisterAttempt(float result)
        {
            if (TotalAttempts == 0)
            {
                curSuccessLevel = result * SuccessLevelHistory;
            }
            else
            {
                curSuccessLevel -= curSuccessLevel / (float)SuccessLevelHistory;
                curSuccessLevel += result;
            }

            TotalAttempts++;
        }
    }
}
